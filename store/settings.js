export const state = () => ({});

export const mutations = {
  set(_state, data) {
    _state.title = data.title;
    _state.description = data.description;
    _state.keywords = data.keywords;
    _state.favicon = data.favicon;
  },
};
