import createNestedObject from '../services/create-nested-object';

export const state = () => ({});

export const mutations = {
  set(_state, data) {
    const _data = { ...data, fetched: true };
    const newState = createNestedObject(_state, data.nameArr, _data);

    Object.assign(_state, newState);
  },
};
  