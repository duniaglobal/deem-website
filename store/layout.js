/* eslint-disable import/prefer-default-export */

export const state = () => ({
  cache: {},
  bottomBar: {
    hideRequest: [],
    forceHide: false,
  },
  header: {
    background: '',
  },
});

export const mutations = {
  /**
   * Removes a `hideRequest` of the `bottomBar.hideRequest` that matches the
   * given `_uid`.
   *
   * @param {Object} _state
   * @param {Number} _uid
   */
  showBottomBarRequest(_state, _uid) {
    const { hideRequest } = _state.bottomBar;
    const index = hideRequest.indexOf(_uid);

    if (index >= 0) {
      hideRequest.splice(index, 1);
    }
  },
  /**
   * Adds the give `_uid` to the `botomBar.hideRequest` array.
   *
   * @param {Object} _state
   * @param {Number} _uid
   */
  hideBottomBarRequest(_state, _uid) {
    const { hideRequest } = _state.bottomBar;

    if (hideRequest.includes(_uid) === false) {
      hideRequest.push(_uid);
    }
  },

  forceHideBottomBar(_state, hide) {
    _state.bottomBar.forceHide = hide;
  },

  setHeaderBackground(_state, background) {
    _state.header.background = background;
  },

  set(_state, data) {
    const { storeName, layout } = data;
    const dataToSave = layout || _state.cache[storeName];

    //
    if (layout) {
      _state.cache[storeName] = layout;
    }

    //
    _state.bottomBar = {
      hideRequest: _state.bottomBar.hideRequest,
      forceHide: _state.bottomBar.forceHide,
      ...dataToSave.bottomBar,
    };

    _state.footer = {
      ..._state.footer,
      ...dataToSave.footer,
    };

    _state.menu = {
      ..._state.menu,
      ...dataToSave.menu,
    };

    _state.login = {
      ..._state.login,
      ...dataToSave.login,
    };
  },
};
