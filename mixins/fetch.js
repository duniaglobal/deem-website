import getEndpoint from '@/helpers/get-endpoint';
import getNestedObject from '@/helpers/get-nested-object';
import pathToData from '@/helpers/path-to-data';
import removeLastSlash from '@/helpers/remove-last-slash';

export default function (options) {
  const forceHideBottomBar = options && options.forceHideBottomBar
    ? !!options.forceHideBottomBar
    : false;

  return {
    async fetch(context) {
      const { store, route } = context;
      const { path } = route;

      const endpoint = getEndpoint(context);

      const dataRoute = removeLastSlash(pathToData(path));
      const pageStore = getNestedObject(dataRoute, store.state, '/');
      const splitDataRoute = dataRoute.split('/');
      const storeName = splitDataRoute[0];
      const storeArgs = splitDataRoute.slice(1);

      store.commit('layout/forceHideBottomBar', !!forceHideBottomBar);

      if (!pageStore || !pageStore.fetched) {
        await context.app.$fetchManager.createOrGetRequest(dataRoute, storeName, storeArgs, {
          method: 'get',
          url: endpoint,
          params: {
            api_token: context.env.api.apiToken,
          },
        }, context);
      } else {
        store.commit('layout/set', { storeName });
      }
    },
  };
}
