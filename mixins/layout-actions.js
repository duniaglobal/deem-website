const ACTIONS = [
  {
    name: 'show-bottom-bar',
    fn: (store, action, data) => {
      store.commit('layout/showBottomBarRequest', data);
    },
  },
  {
    name: 'hide-bottom-bar',
    fn: (store, action, data) => {
      store.commit('layout/hideBottomBarRequest', data);
    },
  },
];

export default {
  methods: {
    dispatchLayoutAction(action, data) {
      const match = ACTIONS.find(a => a.name === action);

      if (match) {
        match.fn(this.$store, action, data);

        return true;
      }

      return false;
    },
  },
};
