export default {
  data() {
    return {
      currentStep: 0,
    };
  },
  methods: {
    goToNextStep() {
      this.currentStep += 1;
    },
    goToPreviousStep() {
      this.currentStep -= 1;
    },
    isActive(step) {
      return step === this.currentStep;
    },
  },
};
