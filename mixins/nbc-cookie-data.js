export default {
  data() {
    return {
      domain: process.env.nbcCookie.domain,
      expires: process.env.nbcCookie.expires,
    };
  },
};
