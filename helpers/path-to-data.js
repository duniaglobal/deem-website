export default (path) => {
  const storeName = path === '/' ? 'home' : path.substring(1);
  return storeName;
};
