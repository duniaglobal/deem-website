import Cookies from 'js-cookie';

export default () => {
  const  domain = '.deem.io';  //domain is .deem.io
  const  expires= 1; //expires 1 day after creation
  Cookies.set('GATrackingId', `UA-148233917-1`, {
    secure: true,
    domain,
    expires: parseInt(expires, 10) || 7, 
  });
};
