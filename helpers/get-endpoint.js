// import removeLastSlash from '@/helpers/remove-last-slash';
import expandParams from '@/helpers/expand-params';

/**
 * Given an endpoint, returns a corresponding URL.
 *
 * @param {String} page The requested endpoint
 * @param {Object} context The nuxt context where the endpoins are stored
 * @returns {String} URL if endpoint exists, empty string if does not
 */

const getEndpoint = (context) => {
  const { route } = context;
  const { path } = route.matched[0];
  const { baseUrl } = context.env.api;
  const match = context.env.endpoints[path.replace(/\?$/, '')];
  const endpoint = expandParams(match, route.params);
  const url = `${baseUrl}${endpoint}`;

  return url;
};

export default getEndpoint;
