const saveToStore = (store, storeName, storeArgs, data) => {
  const { layout, settings } = data;

  delete data.layout;
  delete data.settings;

  store.commit('layout/set', { storeName, layout });
  store.commit('settings/set', settings);
  store.commit(`${storeName}/set`, { ...data, nameArr: storeArgs });
};

export default saveToStore;
