/**
 * Given an object, it takes the entity and slug params within the object and returns
 * built href from the intenal URLs reference
 *
 * @param {Object} To look inside for the entity and slug params
 * @param {Object} The internal URLs
 * @returns {String} href built from the entity and slug
 */
const entitySlugToHref = (link, internalUrls) => {
  const { entity, slug } = link;
  const transformedEntity = entity === 'pages' ? `${entity}/${slug}` : entity;

  let href = internalUrls[transformedEntity] || '#';

  if (href) {
    href = href.replace(':param', slug);
  }

  return href;
};

/**
 * Iterates an array and checks if the item is an object or another
 * array and calls the relevant parser
 *
 * @param {Array} array to parse
 * @param {Object} Internal Urls
 * @returns {Array} The parsed array
 */
const parseArray = (arr, internalUrls) => {
  const _arr = [...arr];

  _arr.forEach((item, index, theArray) => {
    if (Array.isArray(item)) {
      theArray[index] = parseArray(item, internalUrls);
    } else if (typeof item === 'object') {
      // eslint-disable-next-line no-use-before-define
      theArray[index] = parseObj(item, internalUrls);
    }
  });

  return _arr;
};

/**
 * Iterates and object and checks if the item is another object or an
 * array and calls the relevant parser. Checks whether the passed object has an
 * entity param and if so then calls entitySlugToHref to create the href.
 *
 * @param {Ob,ect} object to parse
 * @param {Object} Internal Urls
 * @returns {Object} The parsed object
 */
const parseObj = (obj, internalUrls) => {
  const _obj = { ...obj };

  if (_obj.link) {
    _obj.href = entitySlugToHref(_obj.link, internalUrls);
  }

  if (_obj.links) {
    _obj.links.forEach((item, index) => {
      _obj.links[index].href = entitySlugToHref(item, internalUrls);
    });
  }

  Object.keys(_obj).forEach((key) => {
    let item = _obj[key];

    if (Array.isArray(item)) {
      item = parseArray(item, internalUrls);
    } else if (typeof item === 'object' && !!item) {
      item = parseObj(item, internalUrls);
    }

    _obj[key] = item;
  });

  return _obj;
};

/**
 * Given an ....
 *
 * @param {String} page The requested endpoint ....
 * @param {Object} context The nuxt context where the endpoins are stored ...
 * @returns {String} URL if endpoint exists, empty string if does not ...
 */
const hrefBuilder = (json, context) => {
  if (!json) return {};

  const { internalUrls } = context.env;

  const _json = parseObj(json, internalUrls);

  return _json;
};

export default hrefBuilder;
