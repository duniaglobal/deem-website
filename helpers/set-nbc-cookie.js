import Cookies from 'js-cookie';

export default (name, domain, expires) => {
  Cookies.set('nbc', `firstName=${name}`, {
    secure: true,
    domain,
    expires: parseInt(expires, 10) || 7,
  });
};
