const expandParams = (path, params) => {
  let expandedPath = path;

  Object.keys(params).forEach((param) => {
    expandedPath = expandedPath.replace(`:${param}`, params[param]);
  });

  return expandedPath;
};

export default expandParams;
