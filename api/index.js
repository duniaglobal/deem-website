export default (req, res) => {
  const file = req.url.slice(1);
  const json = require(`./data/${file}.json`);

  res.end(JSON.stringify(json));
};
