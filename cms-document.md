# CMS Document

## blog

### [Insights](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/insights)

```
@prop {Input} title
@prop {BlogCover[]} posts
```

## blog/_post

### [BlogPost](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/components/article/blog-post)
```
@prop {Input} title
@prop {Datepicker} date
@prop {Wysiwyg} content
@prop {Input[]} footerContents
@prop {Input} facebookUrl
@prop {Input} twitterUrl
@prop {Input} mailUrl
```

Also a cover image should be provided so it can be used on `BlogCover` components:

```
@prop {Medias} image 784x1040
```

### [Promo](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/promo)
```
@prop {Input} legend
@prop {Select} background
@prop {Medias} backgroundImage 1440x1784
@prop {Input} anchor
@prop {Input} href
@prop {Input} title
```

`background` can be:

| Value         | Description   |
| :--- |:--- |
| **blue** | Blue background (default) |
| gold | Gold background |


### [FurtherReading](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/further-reading)
```
@prop {Input} legend
@prop {BlogCover[]} posts
```


## cards/_card

### [ProductHero](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/product-hero)
```
@prop {Input} title
@prop {Wysiwyg} text
@prop {Input} anchor
@prop {Input} href
@prop {Medias[]} pictures 980x1600
```

### [Benefits](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/benefits)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Medias} items[].image 1000x882
@prop {Input} items[].content
```

### [ColumnItems](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/column-items)
```
@prop {Input} title
@prop {Repeater} options
@prop {Input} options[].title
@prop {Input} options[].description
@prop {Medias} options[].image 728x___
```

### [Faqs](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/faqs)
```
@prop {Input} legend
@prop {Input} anchor
@prop {Input} href
@prop {Repeater} questions
@prop {Input} questions[].question
@prop {Input} questions[].answer
@prop {Input} title
```

### [Nbc](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/nbc)
```
@prop {Input} legend
@prop {Medias} backgroundImage 2880x1784
@prop {Input} anchor
@prop {Input} openingText
@prop {Input} closingText
@prop {Input} nbcUrl
@prop {Input} dropdownPlaceholder
@prop {Repeater} dropdownOptions
@prop {Input} dropdownOptions[].value
@prop {Input} dropdownOptions[].label
@prop {Input} selectedProduct
@prop {Input} selectedProductValue
```

### [RelatedProducts](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/related-products)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Color} items[].bacgroundColor
@prop {Medias} items[].image 1040x1360
@prop {Input} items[].anchor
@prop {Input} items[].href
@prop {Input} items[].content
```

## daily-needs/_product

### [ProductHero](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/product-hero)
```
@prop {Input} title
@prop {Wysiwyg} text
@prop {Input} anchor
@prop {Input} href
@prop {Medias[]} pictures 980x1600
```

### [Benefits](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/benefits)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Medias} items[].image 1000x882
@prop {Input} items[].content
```

### [TextPicture](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/text-picture)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Medias} options[].image 728x___
@prop {Input} items[].description
```

### [Faqs](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/faqs)
```
@prop {Input} legend
@prop {Input} anchor
@prop {Input} href
@prop {Repeater} questions
@prop {Input} questions[].question
@prop {Input} questions[].answer
@prop {Input} title
```

### [Nbc](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/nbc)
```
@prop {Input} legend
@prop {Medias} backgroundImage 2880x1784
@prop {Input} anchor
@prop {Input} openingText
@prop {Input} closingText
@prop {Input} nbcUrl
@prop {Input} dropdownPlaceholder
@prop {Repeater} dropdownOptions
@prop {Input} dropdownOptions[].value
@prop {Input} dropdownOptions[].label
@prop {Input} selectedProduct
@prop {Input} selectedProductValue
```

### [RelatedProducts](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/related-products)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Color} items[].bacgroundColor
@prop {Medias} items[].image 1040x1360
@prop {Input} items[].anchor
@prop {Input} items[].href
@prop {Input} items[].content
```

## help

### [HelpHero](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/help-hero)
```
@prop {Input} title
@prop {Input} label
```

### [HelpCategories](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/help-categories)
```
@prop {Input} seeAllAnchor
@prop {Repeater} items
@prop {Input} items[].href
@prop {Input} items[].anchor
@prop {Input} items[].image
@prop {Repeater} items[].links
@prop {Input} items[].links[].href
@prop {Input} items[].links[].anchor

```

### [Promo](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/promo)
```
@prop {Input} legend
@prop {Select} background
@prop {Medias} backgroundImage 1440x1784
@prop {Input} anchor
@prop {Input} href
@prop {Input} title
```
`background`'s values:

| Value         | Description   |
| :--- |:--- |
| **blue** | Blue background (default) |
| gold | Gold background |

### [HightlightedArticles](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/highlighted-articles)
```
@prop {Input} legend
@prop {Repeater} links
@prop {Input} links[].href
@prop {Input} links[].anchor
```

## help/articles/_article

### [HelpSidebar](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/help-sidebar)
```
@prop {Repeater} links
@prop {Input} links[].href
@prop {Input} links[].anchor
```

### [HelpArticle](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/help-article)
```
@prop {Input} title
@prop {Wysiwyg} content
@prop {Input} relatedTitle
@prop {Repeater} relatedArticles
@prop {Input} relatedArticles[].href
@prop {Input} relatedArticles[].anchor
@prop {Input} questionsTitle
@prop {Wysiwyg} questionsContent
```

## help/categories/_category

### [HelpSidebar](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/help-sidebar)
```
@prop {Repeater} links
@prop {Input} links[].href
@prop {Input} links[].anchor
```

### [HelpCategory](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/help-category)
```
@prop {Input} title
@prop {Repeater} links
@prop {Input} links[].href
@prop {Input} links[].anchor
@prop {Input} questionsTitle
@prop {Wysiwyg} questionsContent
```

## insights/_id

-

## insurances/_insurance

### [ProductHero](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/product-hero)
```
@prop {Input} title
@prop {Wysiwyg} text
@prop {Input} anchor
@prop {Input} href
@prop {Medias[]} pictures 980x1600
```

### [Benefits](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/benefits)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Medias} items[].image 1000x882
@prop {Input} items[].content
```

### [Features](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/features)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Medias} items[].image 568x722
@prop {Input} items[].title
@prop {Input} items[].content
```

### [Nbc](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/nbc)
```
@prop {Input} legend
@prop {Medias} backgroundImage 2880x1784
@prop {Input} anchor
@prop {Input} openingText
@prop {Input} closingText
@prop {Input} nbcUrl
@prop {Input} dropdownPlaceholder
@prop {Repeater} dropdownOptions
@prop {Input} dropdownOptions[].value
@prop {Input} dropdownOptions[].label
@prop {Input} selectedProduct
@prop {Input} selectedProductValue
```

### [RelatedProducts](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/related-products)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Color} items[].bacgroundColor
@prop {Medias} items[].image 1040x1360
@prop {Input} items[].anchor
@prop {Input} items[].href
@prop {Input} items[].content
```

### [Faqs](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/faqs)
```
@prop {Input} legend
@prop {Input} anchor
@prop {Input} href
@prop {Repeater} questions
@prop {Input} questions[].question
@prop {Input} questions[].answer
@prop {Input} title
```


## loans/_loan

### [ProductHero](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/product-hero)
```
@prop {Input} title
@prop {Wysiwyg} text
@prop {Input} anchor
@prop {Input} href
@prop {Medias[]} pictures 980x1600
```

### [Benefits](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/benefits)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Medias} items[].image 1000x882
@prop {Input} items[].content
```

### [Features](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/features)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Medias} items[].image 568x722
@prop {Input} items[].title
@prop {Input} items[].content
```

### [Nbc](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/nbc)
```
@prop {Input} legend
@prop {Medias} backgroundImage 2880x1784
@prop {Input} anchor
@prop {Input} openingText
@prop {Input} closingText
@prop {Input} nbcUrl
@prop {Input} dropdownPlaceholder
@prop {Repeater} dropdownOptions
@prop {Input} dropdownOptions[].value
@prop {Input} dropdownOptions[].label
@prop {Input} selectedProduct
@prop {Input} selectedProductValue
```

### [RelatedProducts](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/related-products)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Color} items[].bacgroundColor
@prop {Medias} items[].image 1040x1360
@prop {Input} items[].anchor
@prop {Input} items[].href
@prop {Input} items[].content
```

### [Faqs](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/faqs)
```
@prop {Input} legend
@prop {Input} anchor
@prop {Input} href
@prop {Repeater} questions
@prop {Input} questions[].question
@prop {Input} questions[].answer
@prop {Input} title
```


## company

### [Spy](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/components/containers/spy)

No attributes, content is set on the default slot.

### [PlainText](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/components/article/plain-text)
```
@prop {Input} content
```
Content can be set on the default slot too.

### [Team](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/team)
```
@prop {Input} title
@prop {Repeater} members
@prop {Medias} members[].image 784x788
@prop {Input} members[].name
@prop {Input} members[].position
@prop {Input} members[].description
```

### [Promo](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/promo)
```
@prop {Input} legend
@prop {Select} background
@prop {Medias} backgroundImage 1440x1784
@prop {Input} anchor
@prop {Input} href
@prop {Input} title
```

`background`'s values:

| Value         | Description   |
| :--- |:--- |
| **blue** | Blue background (default) |
| gold | Gold background |

## faqs

-

## home

### [MainHero](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/main-hero)
```
@prop {Repeater} slides
@prop {Input} slides[].title
@prop {Input} slides[].text
@prop {Input} slides[].anchor
@prop {Input} slides[].href
@prop {Repeater} slides[].mosaicItems
@prop {Medias} slides[].mosaicItems[].image 636x952
@prop {Medias} slides[].mosaicItems[].video
```

### [BrandValues](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/brand-values)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Medias} items[].image 1000x882
@prop {Input} items[].content
```

`background`'s values:

| Value         | Description   |
| :--- |:--- |
|   | Regular section (default) |
| dark | Dark background with white text |
| blue | Blue background |
| gold | Gold background |
| transparent | Transparent background |




### [TextPicture](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/modules/text-picture)
```
@prop {Input} legend
@prop {Repeater} items
@prop {Input} items[].title
@prop {Medias} options[].image 728x___
@prop {Input} items[].description
```

### [Promo](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/promo)
```
@prop {Input} legend
@prop {Select} background
@prop {Medias} backgroundImage 1440x1784
@prop {Input} anchor
@prop {Input} href
@prop {Input} title
```
`background`'s values:

| Value         | Description   |
| :--- |:--- |
| **blue** | Blue background (default) |
| gold | Gold background |

### [Nbc](https://repos.codehanzo.com/dunia/dunia-ui/tree/develop/src/recipes/nbc)
```
@prop {Input} legend
@prop {Medias} backgroundImage 2880x1784
@prop {Input} anchor
@prop {Input} openingText
@prop {Input} closingText
@prop {Input} nbcUrl
@prop {Input} dropdownPlaceholder
@prop {Repeater} dropdownOptions
@prop {Input} dropdownOptions[].value
@prop {Input} dropdownOptions[].label
@prop {Input} selectedProduct
@prop {Input} selectedProductValue
```

## terms

-
