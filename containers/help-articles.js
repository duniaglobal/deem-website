export default {
  render() {
    return this.$scopedSlots.default({
      onClickArticle(path) {
        this.$router.push({
          path,
        });
      },
    });
  },
};
