export default {
  render() {
    return this.$scopedSlots.default({
      onClick(path) {
        this.$router.push({
          path,
        });
      },
    });
  },
};
