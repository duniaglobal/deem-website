export default {
  render() {
    return this.$scopedSlots.default({
      onButtonClick(path) {
        window.open(path);
      },
    });
  },
};
