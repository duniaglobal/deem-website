import { ViewportObserver } from '@dunia/ui';
import LayoutActions from '@/mixins/layout-actions';

export default {
  mixins: [ViewportObserver, LayoutActions],
  props: {
    // Show the bottom bar until it starts intersecting the viewport
    thresholds: {
      type: Array,
      default: () => [
        {
          threshold: 0,
          action: 'hide-bottom-bar',
        },
        {
          threshold: 'below',
          action: 'show-bottom-bar',
        },
      ],
    },
  },
  // This overrides `inViewportHandler` defined on `ViewportObserver` mixin.
  methods: {
    inViewportHandler(threshold) {
      this.dispatchLayoutAction(threshold.action, this._uid);
    },
  },
  // Set the `BottomBar` free.
  beforeDestroy() {
    this.dispatchLayoutAction('show-bottom-bar', this._uid);
  },
  render() {
    return this.$scopedSlots.default({});
  },
};
