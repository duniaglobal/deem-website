import scrollTo from 'scroll-to';

export default {
  render() {
    return this.$scopedSlots.default({
      onChange(active) {
        const { commit, state } = this.$store;

        if (active >= 0) {
         if(window.location.href.includes('/referral'))
          {
            const { background } = state.referral.mainHero.slides[active];
            commit('layout/setHeaderBackground', background);
          }
          else
          {
            const { background } = state.home.mainHero.slides[active];
            commit('layout/setHeaderBackground', background);
          }

        }
      },
      onClick(path) {
        if (path.startsWith('https://') || path.startsWith('http://')) {
          window.open(path, '_blank');
        } else if (path.startsWith('#')) {
          const $target = document.querySelector(path);
          if ($target) {
            const tgTop = $target.getBoundingClientRect().top;
            const doc = document.documentElement || document.body;
            const scrTop = doc.scrollTop;
            const to = tgTop + scrTop;
            scrollTo(0, to);
          }
        } else {
          this.$router.push({ path });
        }
      },
    });
  },
};
