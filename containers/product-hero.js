export default {
  render() {
    return this.$scopedSlots.default({
      onClick(path) {
        window.open(path);
      },
    });
  },
};
