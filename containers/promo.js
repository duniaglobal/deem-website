export default {
  render() {
    return this.$scopedSlots.default({
      onClick(path) {
        if (path.indexOf('mailto:') === 0) {
          window.location.href = path;
        } else {
          this.$router.push({
            path,
          });
        }
      },
    });
  },
};
