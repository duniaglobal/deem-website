export default {
  render() {
    return this.$scopedSlots.default({
      onSubmit(searchTerm) {
        const path = `/help/search/${searchTerm}`;
        this.$router.push({
          path,
        });
      },
    });
  },
};
