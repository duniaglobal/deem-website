import { ViewportObserver } from '@dunia/ui';
import LayoutActions from '@/mixins/layout-actions';
import setNbcCookie from '@/helpers/set-nbc-cookie';
import NbcCookieData from '@/mixins/nbc-cookie-data';

export default {
  mixins: [ViewportObserver, LayoutActions, NbcCookieData],
  props: {
    // Show the BottomBar until the Nbc intersects with the viewport. Keep it
    // hidden when the Nbc goes above the viewport.
    thresholds: {
      type: Array,
      default: () => [
        {
          threshold: 'below',
          action: 'show-bottom-bar',
        },
        {
          threshold: 0,
          action: 'hide-bottom-bar',
        },
        {
          threshold: 'above',
          action: 'hide-bottom-bar',
        },
      ],
    },
  },
  // This overrides `inViewportHandler` defined on `ViewportObserver` mixin.
  methods: {
    inViewportHandler(threshold) {
      this.dispatchLayoutAction(threshold.action, this._uid);
    },
  },
  // Set the `BottomBar` free.
  beforeDestroy() {
    this.dispatchLayoutAction('show-bottom-bar', this._uid);
  },
  render() {
    return this.$scopedSlots.default({
      onButtonClick(name) {
        let nbc;
        let dropdownItem;
        setNbcCookie(name, this.domain, this.expires);

        if (this.selectedProduct) {
          nbc = this.selectedProduct.nbcUrl;
        } else {
          dropdownItem = this.dropdownOptions.find(obj => obj.value === this.dropdownValue);
          nbc = dropdownItem.nbcUrl;
        }

        const urlString = window.location.href;
        if (urlString.includes('?')) {
          if (urlString.includes('?gclid=')) {
            const temp = `${nbc}?utm_source=Google`;
            nbc = temp;
          } else {
            const res = urlString.split('?');
            const temp = `${nbc}?${res[1]}`;
            nbc = temp;
          }
        }
	 else {
          if (urlString.includes('deposit-product')) {

                        const temp = `${nbc}?utm_source=Deposit-Lead`;
                        nbc = temp;
                    } else {
                        const temp = `${nbc}?utm_source=deem.io`;
                        nbc = temp;
                    }        }

        window.open(nbc);
      },
    });
  },
};
