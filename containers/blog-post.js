export default {

  props: {
    blogTitle: {
      type: String,
    },
    twitterText: {
      type: String,
      default: '',
    },
    mailSubject: {
      type: String,
    },
    mailBody: {
      type: String,
    },
  },

  render() {
    const facebookUrl = process.client ? `https://www.facebook.com/sharer/sharer.php?u=#${window.location.href}` : '';
    const twitterUrl = process.client ? `http://twitter.com/share?url=${window.location.href}&text=${this.$props.twitterText} ${this.$props.blogTitle}` : '';
    const mailUrl = process.client ? `mailto:?subject=${this.$props.mailSubject}&amp;body=${this.$props.mailBody} ${window.location.href}` : '';

    return this.$scopedSlots.default({
      facebookUrl,
      twitterUrl,
      mailUrl,
    });
  },
};
