import setNbcCookie from '@/helpers/set-nbc-cookie';
import NbcCookieData from '@/mixins/nbc-cookie-data';

export default {
  mixins: [NbcCookieData],

  render() {
    const { domain, expires } = this;

    return this.$scopedSlots.default({
      onButtonClick() {
        return (name, nbcUrl) => {
          let nbc = nbcUrl;
          setNbcCookie(name, domain, expires);

          const urlString = window.location.href;
          if (urlString.includes('?')) {
            if (urlString.includes('?gclid=')) {
              const temp = `${nbc}?utm_source=Google`;
              nbc = temp;
            } else {
              const res = urlString.split('?');
              const temp = `${nbc}?${res[1]}`;
              nbc = temp; 
            }
          }
	
	 else {
            if (urlString.includes('deposit-product')) {

                        const temp = `${nbc}?utm_source=Deposit-Lead`;
                        nbc = temp;
                    } else {
                        const temp = `${nbc}?utm_source=deem.io`;
                        nbc = temp;
                    }          }
          window.open(nbc);
        };
      },
    });
  },
};
