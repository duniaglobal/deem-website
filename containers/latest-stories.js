export default {
  render() {
    return this.$scopedSlots.default({
      onClickPost(path) {
        this.$router.push({
          path,
        });
      },
      onClickAction(path) {
        this.$router.push({
          path,
        });
      },
    });
  },
};
