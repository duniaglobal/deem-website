export default {
  render() {
    return this.$scopedSlots.default({
      onClickItem(path) {
        this.$router.push({
          path,
        });
      },
    });
  },
};
