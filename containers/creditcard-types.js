export default {
  render() {
    return this.$scopedSlots.default({
      onButtonClick(path) {
        this.$router.push({
          path,
        });
      },
    });
  },
};
