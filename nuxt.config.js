/* eslint-disable no-useless-escape */
import endpoints from './config/endpoints';
import internalUrls from './config/internal-urls';
import stage from './config/stage';

//
require('dotenv').config();

const setSvgRules = (config) => {
  // Exclude embedded SVG from default image loader
  config.module.rules.find((rule, index) => {
    if (rule.test.toString().indexOf('|svg|') >= 0) {
      rule.exclude = new RegExp('\\-embedded.svg$');
      config.module.rules[index] = rule;
      return true;
    }

    return false;
  });

  // Add rule for embedded SVG
  config.module.rules.push({
    test: /\-embedded.svg$/,
    use: [
      {
        loader: 'babel-loader',
        options: {
          presets: ['@vue/babel-preset-app'],
        },
      },
      {
        loader: 'vue-svg-loader',
      },
    ],
  });
};

module.exports = {
  server: {
    host: process.env.APP_HOST,
    port: process.env.APP_PORT,
  },
  env: {
    NODE_ENV: 'production',
    endpoints,
    internalUrls,
    stage,
    api: {
      baseUrl: process.env.API_BASE_URL,
      apiToken: process.env.API_TOKEN,
    },
    nbcCookie: {
      domain: process.env.NBC_COOKIE_DOMAIN,
      expires: process.env.NBC_COOKIE_EXPIRES,
    },
  },
  modulesDir: ['~/node_modules'],
  plugins: [
    { src: '~/plugins/modernizr.js', ssr: false },
    { src: '~/plugins/fetch-manager.js' },
    { src: '~/plugins/intersection-observer.js', ssr: false },
    { src: '~/plugins/ga.js', ssr: false },
    { src: '~/plugins/gtag.js', ssr: false },
  ],
  build: {
    extractCSS: true,
    extend(config, ctx) {
      config.devtool = ctx.isClient ? 'eval-source-map' : 'inline-source-map';

      setSvgRules(config);

      config.module.rules.push({
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          presets: ['@vue/babel-preset-app'],
        },
        include: /(@dunia\/ui)/,
      });

      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }

      if (ctx.isClient) {
        config.module.rules.push({
          test: /\.modernizrrc(\.json)?$/,
          use: ['modernizr-loader', 'json-loader'],
        });

        config.resolve.alias.modernizr = '~/.modernizrrc';
      }

      if (ctx.isServer) {
        config.module.rules.push({
          test: /flickity(.*)\.js/,
          use: ['ignore-loader'],
        });

        config.module.rules.push({
          test: /lottie-web/,
          use: ['ignore-loader'],
        });

        config.module.rules.push({
          test: /modernizr/,
          use: ['ignore-loader'],
        });

        config.module.rules.push({
          test: /vue-images-loaded(.*)\.js/,
          use: ['ignore-loader'],
        });

        config.module.rules.push({
          test: /ua\.js/,
          use: ['ignore-loader'],
        });

        config.module.rules.push({
          test: /scroll-to(.*)\.js$/,
          use: ['ignore-loader'],
        });

        config.module.rules.push({
          test: /detections\//,
          use: ['ignore-loader'],
        });
      }
    },
  },

  router: {
    linkActiveClass: 'current',
  },
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ],
  },
  loading: '~/components/loading.vue',
  serverMiddleware: [
    { path: '/api', handler: '~/api/index' },
  ],
  generate: {
    interval: 10,
    routes: [
    ],
  },
};
