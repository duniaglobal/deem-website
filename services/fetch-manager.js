import axios from 'axios';
import hrefBuilder from '@/helpers/href-builder';
import saveToStore from '@/helpers/save-to-store';

class FetchManager {
  constructor() {
    this.__promises = FetchManager.savePromise ? [] : false;
  }

  static get savePromise() {
    return true;
  }

  shouldReturnPromiseFor(name) {
    if (FetchManager.savePromise) {
      return !!this.__promises[name];
    }

    return false;
  }

  savePromiseFor(name, promise) {
    if (FetchManager.savePromise) {
      this.__promises[name] = promise;
    }
  }

  createOrGetRequest(name, storeName, storeArgs, axiosProps, context) {
    const { store, error } = context;

    if (this.shouldReturnPromiseFor(name)) {
      return this.__promises[name];
    }

    const pr = axios(axiosProps);

    pr.then((res) => {
      if (!res.data.error && (res.status === 200 || res.status === 304)) {
        const data = hrefBuilder(res.data, context);

        saveToStore(store, storeName, storeArgs, data);
      } else {
        error({
          statusCode: res.data.error.status_code || res.status,
        });
      }
    }, (e) => {
      error({
        statusCode: e.response.status,
      });
    });

    this.savePromiseFor(name, pr);

    return pr;
  }
}

export default () => new FetchManager();
