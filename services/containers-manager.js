import Vue from 'vue';
import * as containers from '../containers';


export default (component, instance = '') => {
  const container = `${component}${instance}Container`;

  if (containers[container]) {
    Vue.component(container, containers[container]);
    return containers[container];
  }

  return containers.EmptyContainer;
};
