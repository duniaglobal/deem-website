const nest = (obj, keys, v) => {
  const _obj = { ...obj };

  if (!keys || !keys.length) {
    return { ..._obj, ...v };
  }

  if (keys.length === 1) {
    _obj[keys[0]] = v;
  } else {
    const key = keys.shift();
    _obj[key] = nest(typeof _obj[key] === 'undefined' ? {} : _obj[key], keys, v);
  }

  return _obj;
};

export default nest;
