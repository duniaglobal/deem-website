const internalUrls = {};

internalUrls['products/loans'] = '/loans/:param';
internalUrls['products/cards'] = '/cards/:param';
internalUrls['products/daily-needs'] = '/daily-needs/:param';
internalUrls['products/insurances'] = '/insurances/:param';
internalUrls['products/corporate'] = '/corporate/:param';
internalUrls['products/corporates'] = '/corporate/:param';
internalUrls['products/prepaid'] = '/prepaid/:param';
internalUrls['products/protects'] = '/protects/:param';


internalUrls['pages/home'] = '/';
internalUrls['pages/company'] = '/company';
internalUrls['pages/coverage'] = '/coverage';
internalUrls['pages/blog'] = '/blog';
internalUrls['pages/help'] = '/help';
internalUrls['pages/terms'] = '/terms';
internalUrls['pages/consent'] = '/consent';
internalUrls['pages/faqs'] = '/faqs';
internalUrls['pages/forms'] = '/forms';
internalUrls['pages/fees'] = '/fees';
internalUrls['pages/contact'] = '/contact';
internalUrls['pages/branchapply'] = '/branchapply';
internalUrls['pages/locations'] = '/locations';
internalUrls['pages/worldmilesupterms'] = '/worldmilesupterms';
internalUrls['pages/platinummilesupterms'] = '/platinummilesupterms';
internalUrls['pages/titaniummilesupterms'] = '/titaniummilesupterms';
internalUrls['pages/worldcashupterms'] = '/worldcashupterms';
internalUrls['pages/platinumcashupterms'] = '/platinumcashupterms';
internalUrls['pages/titaniumcashupterms'] = '/titaniumcashupterms';
internalUrls['pages/referral'] = '/referral';
internalUrls['pages/charter'] = '/charter';
internalUrls['blog/post'] = '/blog/:param';

internalUrls['help/category'] = '/help/categories/:param';
internalUrls['help/article'] = '/help/articles/:param';

export default internalUrls;
