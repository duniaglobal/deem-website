const endpoints = {};

endpoints[''] = '/pages/home';
endpoints['/company'] = '/pages/company';
endpoints['/coverage'] = '/pages/coverage';
endpoints['/help'] = '/pages/help';
endpoints['/blog'] = '/pages/blog';
endpoints['/terms'] = '/pages/terms';
endpoints['/faqs'] = '/pages/faqs';
endpoints['/consent'] = '/pages/consent';
endpoints['/forms'] = '/pages/forms';
endpoints['/fees'] = '/pages/fees';
endpoints['/contact'] = '/pages/contact';
endpoints['/branchapply'] = '/pages/branchapply';
endpoints['/locations'] = '/pages/locations';
endpoints['/worldmilesupterms'] = '/pages/worldmilesupterms';
endpoints['/platinummilesupterms'] = '/pages/platinummilesupterms';
endpoints['/titaniummilesupterms'] = '/pages/titaniummilesupterms';
endpoints['/worldcashupterms'] = '/pages/worldcashupterms';
endpoints['/platinumcashupterms'] = '/pages/platinumcashupterms';
endpoints['/titaniumcashupterms'] = '/pages/titaniumcashupterms';
endpoints['/referral'] = '/pages/referral';
endpoints['/charter'] = '/pages/charter';
endpoints['/cards/:card'] = '/products/cards/:card';
endpoints['/insurances/:insurance'] = '/products/insurances/:insurance';
endpoints['/loans/:loan'] = '/products/loans/:loan';
endpoints['/prepaid/:prepaid'] = '/products/prepaid/:prepaid';
endpoints['/protects/:protect'] = '/products/protects/:protect';

endpoints['/corporate/:corporate'] = '/products/corporate/:corporate';

endpoints['/blog/:post'] = '/blog/posts/:post';

endpoints['/help/articles/:article'] = '/help/article/:article';
endpoints['/help/categories/:category'] = '/help/category/:category';

export default endpoints;
