# Dunia website

This is the repository for the Dunia public website.

## Requirements

- [Node 10.9.0](https://nodejs.org/es/)

Having installed [NVM](https://github.com/creationix/nvm) is recommended to execute different NPM versions in the development machine. This projects asumes the Node version is `v10.9.0`.

If you have NPM execute this command in the repository folder for every new terminal session:

```
nvm use
```

## Project setup

```
yarn install
```

This command will install the Node dependencies from `package.json`.

### Development server

```
yarn serve
```

Starts the Nuxt built in SSR server in development mode.

### Lint files

```
yarn lint
```

Lints the Javascript and `.vue` files in the project.

## Production mode

### .env file

The project needs and `.env` file with some environment variables.

```
API_BASE_URL=https://cms-develop.dunia.hanzo.es/api/v1
API_TOKEN=4FsRfI3xbBf89H9iPHv6YyyKnOArbKeqfE4lpMDSQc7NFHUAU7W34uf0FxNT
APP_HOST=0.0.0.0
APP_PORT=3000
NBC_COOKIE_DOMAIN=localhost
NBC_COOKIE_EXPIRES=1
```

These are the vars:

- `API_BASE_URL` - The base URL for the CMS API which publishes the content
- `API_TOKEN` - A valid token to include it in the API http requests.
- `APP_HOST` - The host name that Nuxt will for the server. Use `0.0.0.0` for any URL/domain pointing to the server.
- `APP_PORT` - The port number where Nuxt will publish the server.
- `NBC_COOKIE_DOMAIN` - Domain of the cookie the website uses to send information (the first name) to the NBC.
- `NBC_COOKIE_EXPIRES` - Optional variable, the number of days when the cookie will expire. By default the value is 7.

### Production server

```
yarn servep
```

Starts the Nuxt SSR server in production mode. Production mode includes tree shaking, code splitting, static file saving, transpilation and bundling. 

Please check the  [official documentation](https://nuxtjs.org/guide/commands#production-deployment) for more information.

## nuxt.config.js file tips

This file overwrites the default configuration from Nuxt. Few tips about it.

### embedded svg files

In the UI, some SVGs files are embedded as `Vue` components. For them to be processed correctly by Nuxt, a new Webpack rule is needed. It is already in place and running:

```
config.module.rules.push({
  test: /\-embedded.svg$/,
  use: [
    {
      loader: 'babel-loader',
      options: {
        presets: ['@vue/babel-preset-app'],
      },
    },
    {
      loader: 'vue-svg-loader',
    },
  ],
});
```

### Dependencies using DOM objects

Nuxt runs compiles the code in two different processes, and hence, environments: server and client.

When any dependency in website, or in the UI, uses a DOM object, the server process fails since those objects don't exist in the environment.

To avoid the issue, it is posible to add a Webpack rule to process those dependencies trough the `ignore loader` for the server process.

```
if (ctx.isServer) {
  config.module.rules.push({
    test: /lottie-web/,
    use: ['ignore-loader'],
  });
}
```

## Config folder

The config folder contains files to configure the endpoints urls and the internal links.

### Endpoints URLs

This file contains the match from the website urls to the CMS endpoints.

For example:

```
endpoints['/company'] = '/pages/company';
```

The Nuxt `/company` route will pull the data from the endpoint `/pages/company` from the CMS. That endpoint is concatenated to the `API_BASE_URL` value from the `.env` file.

URLs with parameters are passed through a filter in order to match them in the CMS:

```
endpoints['/cards/:card'] = '/products/cards/:card';
```

A request to `/cards/cash-up` in Nuxt will pull the data from `/products/cards/cash-up` endpoint in the CMS.

### Internal links

In the CMS, the links from one page to other are managed by a local relation. The text `href` inputs are avoided as much as posible in order to keep the robustness of the website.

Those links are published in the CMS API with an object named `link` (or an array of objects named `links`), with usually two props inside: `slug` and `entity`:

```
{
  entity: "products/cards",
  slug: "cash-up"
}
```

Nuxt is converting that object into a proper link using the `internal urls` file.

```
internalUrls['products/cards'] = '/cards/:param';
```

The `entity` corresponds to the array index. Its value is passed through a filter that returns the correct URL after replacing the param for the `slug` content:

```
/cards/cash-up
```

## Middleware API

Nuxt provides a middleware API server that can be used for development purposes if the CMS is not in place.

The `api/index.js` file contains a small code to parse any request to `/api/*` and look for json files in the `data` folder.

This middleware server is still configured in `nuxt.config.js` file, since it could be useful in the future, but currently it is unused since the website pulls the data directly from the CMS.