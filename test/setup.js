const hooks = require('require-extension-hooks');
const env = require('browser-env');

// Enable browser-env only on Unit Testing
// If browser-env is enabled during e2e tests, the app will crash
if (process.env.TEST === 'unit') {
  env();
}

hooks('vue')
  .plugin('vue')
  .push();
hooks(['vue', 'js'])
  .plugin('babel')
  .push();
