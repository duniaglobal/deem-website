import test from 'ava';
import { Nuxt, Builder } from 'nuxt';
import { resolve } from 'path';

// We keep a reference to Nuxt so we can close
// the server at the end of the test
let nuxt = null;

// Init Nuxt.js and start listening on localhost:4000
test.before('Init Nuxt.js', async () => {
  const rootDir = resolve(__dirname, '../../');
  let config = {};
  try {
    config = require(resolve(rootDir, 'nuxt.config.js'));
  } catch (e) {
    console.log(e);
  }
  config.rootDir = rootDir; // project folder
  config.dev = false; // production build
  config.mode = 'universal'; // Isomorphic application
  nuxt = new Nuxt(config);
  await new Builder(nuxt).build();
  nuxt.listen(4000, 'localhost');
});

// Example of testing only generated html
test('Route / exits and render HTML', async (t) => {
  const context = {};
  await nuxt.renderRoute('/', context);
  t.true(true);
});

// Close the Nuxt server
test.after('Closing server', () => {
  nuxt.close();
});
