import fetchManager from '@/services/fetch-manager';

export default (context, inject) => {
  inject('fetchManager', fetchManager());
};
